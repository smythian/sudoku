/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


import java.util.ArrayList;

import javax.swing.JButton;

/**
 * SudokuMove class for the Sudoku game
 *
 * @author Eric Smyth
 */
public class SudokuMove extends JButton
{
	//Private Static Final Fields

	/**
	 * A list of valid entries
	 */
	private static final ArrayList<Integer> VALID = new ArrayList<Integer> (initValid());

	/**
	 * SudokuMove Class Version
	 */
	private static final long serialVersionUID = 1;

	//Private Methods

	/**
	 * The row on the board
	 */
	private int row;

	/**
	 * The column of the board
	 */
	private int col;

	/**
	 * Whether the SudokuMove is modifiable
	 */
	private boolean modifiable;

	//Constructor

	/**
	 * Constructs a new SudokuMove
	 *
	 * @param row The row on the board
	 * @param col The column on the board
	 */
	public SudokuMove (int row, int col)
	{
		this.row = row;
		this.col = col;
		this.modifiable = true;
	}

	//Public Methods

	/**
	 * Clears modifiable Moves
	 */
	public void clear()
	{
		if(modifiable)
		{
			setText("");
		}
	}

	/**
	 * Getter for the value stored by the move
	 *
	 * @return The value of the SudokuMove
	 */
	public int getValue()
	{
		int i = 0;

		if(getText().equals(""))
		{
			i = 0;
		}
		else
		{
			i = Integer.parseInt(getText());
		}

		return i;
	}

	/**
	 * The getter for the column
	 *
	 * @return The column of the SudokuMove
	 */
	public int getCol()
	{
		return col;
	}

	/**
	 * The getter for the row
	 *
	 * @return The row of the SudokuMove
	 */
	public int getRow()
	{
		return row;
	}

	/**
	 * The getter for the modifiable state
	 *
	 * @return The modifiable state of the SudokuMove
	 */
	public boolean isModifiable()
	{
		return modifiable;
	}

	/**
	 * Clears all Moves and restores the board to the initial
	 * state
	 */
	public void reset()
	{
		modifiable = true;
		setFocusable(true);
		setForeground(SudokuGUI.FOREUSER);
		clear();
	}

	/**
	 * Sets the SudokuMove to unmodifiable
	 */
	public void setUnMod()
	{
		setForeground(SudokuGUI.FORESTATIC);
		modifiable = false;
	}

	/**
	 * Sets the value of the SudokuMove
	 *
	 * @param i The value of the SudokuMove
	 */
	public void setValue(int i)
	{
		if(modifiable)
		{
			if(i == 0)
			{
				setText("");
			}
			else if(VALID.contains(i))
			{
				setText(String.valueOf(i));
			}
		}
	}

	/**
	 * Creates a string representation of the SudokuMove
	 */
	public String toString()
	{
		StringBuilder s = new StringBuilder();

		s.append(row);
		s.append(",");
		s.append(col);
		s.append(",");
		s.append(getValue());
		s.append(",");
		s.append(modifiable);

		return s.toString();
	}

	//Private Methods

	/**
	 * The initValid method is used to initialize the VALID
	 * constant
	 *
	 * @return An ArrayList containing the Integers 1-N
	 */
	private static ArrayList<Integer> initValid()
	{
		ArrayList<Integer> temp = new ArrayList<Integer>();

		for(int i = 1; i <= SudokuGUI.N; i++)
		{
			temp.add(i);
		}

		return temp;
	}
} //End of Class SudokuMove
