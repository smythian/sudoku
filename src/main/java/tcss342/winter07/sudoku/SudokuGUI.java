/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Stack;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * The SudokuGUI class is the center of the Sudoku game
 *
 * @author Eric Smyth
 */
public class SudokuGUI extends JFrame
{
	//Public Static Final Fields

	/**
	 * Used for width, height and valid entries, implemented
	 * for possible future conversion to N by N Sudoku
	 */
	public static final int N = getBoardSize();

	//Private Static Final Fields

	/**
	 * Difficulty constants determine the number of squares
	 * pre filled at each difficulty level
	 */
	private static final int EASY = 3*N, HARD = N, MED = 2 * N;

	/**
	 * SudokuGUI Class version
	 */
	private static final long serialVersionUID = 1;

	//Protected Static Final Fields

	/**
	 * The first background color used for even number cubes
	 * (odd index)
	 */
	protected static final Color BACKONE = new Color(83, 45, 115);

	/**
	 * The second background color used for odd number cubes
	 * (even index)
	 */
	protected static final Color BACKTWO = new Color(237, 211, 33);

	/**
	 * The game panel background color
	 */
	protected static final Color BACKTHREE = Color.white;

	/**
	 * The foreground color for squares modifiable by the user
	 */
	protected static final Color FOREUSER = Color.white;

	/**
	 * The move border color
	 */
	protected static final Color BORDERCOLOR = Color.black;

	/**
	 * The foreground color for static squares
	 */
	protected static final Color FORESTATIC = Color.cyan;

	/**
	 * The foreground color for squares that are invalid when
	 * user has selected the hint option from the game menu
	 */
	protected static final Color FOREERROR = Color.green;

	//Private Fields

	/**
	 * The width of the screen
	 */
	private int xwidth;

	/**
	 * The height of the screen
	 */
	private int yheight;

	/**
	 * The game board
	 */
	private SudokuMove[][] board = new SudokuMove[N][N];

	/**
	 * The stack used by the solveStack method to track moves.
	 * Possible future implementation in the solveIntersect
	 * method as well
	 */
	private Stack<SudokuMove> tracker = new Stack<SudokuMove>();

	/**
	 * The file chooser used for load and save operations
	 */
	private JFileChooser choose;

	/**
	 * The file to write to/read from during load save operations
	 */
	private File file;

	/**
	 * Game panel
	 */
	private JPanel gp;

	/**
	 * The menu system
	 */
	private SudokuMenu m;

	/**
	 * Used to obtain the screen resolution for inital positioning
	 * the game at launch
	 */
	private Toolkit kit = Toolkit.getDefaultToolkit();

	/**
	 * Tracks modifications to the puzzle during a game
	 */
	private int modCount = 0;

	/**
	 * Tracks moves by the solver methods
	 */
	private int solveCount = 0;

	/**
	 * Tracks if the user has requested hints
	 */
	private boolean hint = false;

	/**
	 * Tracks the current playtime in milliseconds
	 */
	private long playTime;

	/**
	 * Tracks the time a solver method took to solve the puzzle
	 */
	private long solveTime;

	/**
	 * The initial horizontal position
	 */
	private int xpos;

	/**
	 * The initial vertical position
	 */
	private int ypos;

	/**
	 * The status of the adjacent vertices viewer
	 */
	private boolean vert;

	//Constructor

	/**
	 * Constructs a new Sudoku game
	 */
	public SudokuGUI()
	{
		init();
	}

	//Public Methods

	/**
	 * The about method displays an about dialog to the user
	 */
	public void about()
	{
		String str = "Author: Eric Smyth\nInstructor: Moshe Rosenfeld\nClass: TCSS 342\nVersion date: 14APR07";
		String title = "About - Sudoku v1.5";

		JOptionPane.showMessageDialog(this, str, title, JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Sets the state of the view adjacents vertices
	 * @param state
	 */
	public void adjacent(boolean state)
	{
		vert = state;
	}


	/**
	 * The clear method clears all user specified entries
	 * from the board
	 */
	public void clear()
	{
		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				board[row][col].setBackground(BACKONE);
				board[row][col].clear();
			}
		}
	}

	/**
	 * Sets all the squares on the board to focusable. Intended for
	 * use with solve method
	 */
	public void setFocusAll()
	{
		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				if(board[row][col].isModifiable())
				{
					board[row][col].setFocusable(true);
				}
			}
		}
	}

	/**
	 * The help method displays a help dialog for the user
	 */
	public void help()
	{
		String title = "UW Sudoku v1.5 - Help";

		JOptionPane.showMessageDialog(this, new ImageIcon("src/assets/help.gif"), title, JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * The sets the status of the hint variable which controls
	 * if the user is notified of invalid moves
	 *
	 * @param b True to give hints
	 */
	public void hint(boolean b)
	{
		hint = b;
		Color c;

		if(hint)
		{
			c = FOREERROR;
		}
		else
		{
			c = FOREUSER;
		}

		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				if(board[row][col].isModifiable())
				{
					if(!validMove(row, col, board[row][col].getValue()))
					{

						board[row][col].setForeground(c);
					}
				}
			}
		}
	}

	/**
	 * The loadGame method allows users to load saved games
	 */
	public void loadGame(int i)
	{
		reset();

		choose = new JFileChooser(file);
		choose.setFileSelectionMode(JFileChooser.FILES_ONLY);
		choose.addChoosableFileFilter(new SaveFilter());
		choose.setAcceptAllFileFilterUsed(false);

		if(i == 0)
		{
			if (choose.showOpenDialog(SudokuGUI.this) == JFileChooser.APPROVE_OPTION)
			{
				file = choose.getSelectedFile();
				i = 1;
			}
		}
		else
		{
			file = new File("src/assets/pregen.ssg");
		}

        if(file != null && file.canRead())
		{
			try
			{
				BufferedReader in = new BufferedReader(new FileReader(file));
	        	String str = "";
	        	int count = 0;

	        	while(i > 0)
	        	{
	        		str = in.readLine();
	        		i--;
	        	}

	        	for(int row = 0; row < board.length; row++)
	        	{
	        		for(int col = 0; col < board[row].length; col++)
	        		{
	        			board[row][col].setValue(Integer.parseInt(String.valueOf(str.charAt(count))));
	        			count++;
	        			if(board[row][col].getValue() != 0)
	        			{
	        				board[row][col].setFocusable(false);
	        				board[row][col].setForeground(FORESTATIC);
	        				board[row][col].setUnMod();
	        			}
	        		}
	        	}

	        	count++;

	        	for(int row = 0; row < board.length; row++)
	        	{
	        		for(int col = 0; col < board[row].length; col++)
	        		{
	        			board[row][col].setValue(Integer.parseInt(String.valueOf(str.charAt(count))));
	        			count++;
	        		}
	        	}

	        	if(hint)
	        	{
	        		hint(hint);
	        	}

	        	in.close();
	    	}
			catch (IOException e)
			{
				System.out.println(e);
	    	}
			catch (NumberFormatException e)
			{

			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Invalid File Name/File Not Found\n" + file.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * The new custom game allows the user to specify a
	 * number of elements to be non modifiable. Allows
	 * recreation of Sudoku puzzles seen elsewhere
	 */
	public void newCustom()
	{
		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				if(board[row][col].getValue() != 0)
				{
					board[row][col].setUnMod();
					board[row][col].setFocusable(false);
					board[row][col].setForeground(FORESTATIC);
				}
				board[row][col].clear();
			}
		}

		modCount = 0;
		solveCount = 0;
		playTime = System.currentTimeMillis();

		if(!solvable())
		{
			JOptionPane.showMessageDialog(this, "Your puzzle is unsolvable", "Unsolvable", JOptionPane.ERROR_MESSAGE);
			reset();
		}
		else
		{
			clear();
		}
	}

	/**
	 * Creates a new puzzle by first creating a cayley-sudoku
	 * table and then removing a number of squares
	 *
	 * @param diff The difficulty (1-3)
	 */
	public void newCayley(int diff)
	{
		reset();
		ArrayList<Integer> valid = new ArrayList<Integer>(spaceList());
		int m = (int)Math.sqrt(N);
		int randCol, randRow;
		int count = EASY;

		switch(diff)
		{
			case 1:
				count = (N*N) - EASY;
				break;

			case 2:
				count = (N*N) - MED;
				break;

			case 3:
				count = (N*N) - HARD;
				break;
		}

		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				int temp = (row + ((col%m)*m+(col/m)))%N + 1;
				board[row][col].setValue(temp);
			}
		}

		while(count > 0)
		{
			randRow = valid.get((int)(Math.random() * N));
			randCol = valid.get((int)(Math.random() * N));
			if(board[randRow][randCol].getValue() != 0)
			{
				board[randRow][randCol].setValue(0);
				count--;
			}
		}

		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				if(board[row][col].getValue() != 0)
				{
					board[row][col].setUnMod();
					board[row][col].setFocusable(false);
					board[row][col].setForeground(FORESTATIC);
				}
				board[row][col].clear();
			}
		}

		modCount = 0;
		solveCount = 0;
		playTime = System.currentTimeMillis();
	}

	/**
	 * Creates a new game with the number of pre filled
	 * squares determined by the difficulty. 1 - Easy,
	 * 2 - Medium, 3 - Hard
	 *
	 * @param diff The specified difficulty level
	 */
	public void newGame(int diff)
	{
		reset();

		ArrayList<Integer> valid = new ArrayList<Integer>(spaceList());
		int randRow = 0, randCol = 0, randVal = 0, count = 0;

		switch(diff)
		{
			case 1:
				count = EASY;
				break;

			case 2:
				count = MED;
				break;

			case 3:
				count = HARD;
				break;
		}

		while(count > 0)
		{
			randRow = valid.get((int)(Math.random() * N));
			randCol = valid.get((int)(Math.random() * N));
			randVal = 1 + (int)(Math.random() * N);

			if(validMove(randRow, randCol, randVal))
			{
				board[randRow][randCol].setForeground(FORESTATIC);
				board[randRow][randCol].setValue(randVal);
				board[randRow][randCol].setUnMod();
				board[randRow][randCol].setFocusable(false);
				count--;
			}
		}

		if(!solvable())
		{
			newGame(diff);
		}

		clear();
		modCount = 0;
		solveCount = 0;
		playTime = System.currentTimeMillis();
	}

	/**
	 * The reset method is used to clear all squares on the
	 * board including the unchangable game squares
	 */
	public void reset()
	{
		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				board[row][col].reset();
			}
		}
	}

	/**
	 * The save method provides a primitive way to save
	 * the current state of the board to a file
	 */
	public void save(boolean game)
	{
		String temp = null;
		choose = new JFileChooser(file);
		choose.setFileSelectionMode(JFileChooser.FILES_ONLY);
		choose.addChoosableFileFilter(new SavedGameFilter());
		choose.setAcceptAllFileFilterUsed(false);
		choose.showSaveDialog(SudokuGUI.this);

        file = choose.getSelectedFile();

		if(file != null)
		{
	        temp = file.getName();

			if(temp.length() < 5 || !temp.substring(temp.length() - 4).equalsIgnoreCase(".ssg"))
	        {
	        	temp = file.getAbsolutePath().concat(".ssg");
	        }

			try
			{
	        	BufferedWriter save = new BufferedWriter(new FileWriter(temp));
	        	StringBuilder user = new StringBuilder();
	        	StringBuilder comp = new StringBuilder();

	        	for(int i = 0; i < N; i++)
	        	{
	        		for(int j = 0; j < N; j++)
	        		{
	        			int val = board[i][j].getValue();
	        			if(board[i][j].isModifiable())
	        			{
	        				if(game)
	        				{
	        					user.append(val);
	        				}
	        				else
	        				{
	        					user.append(0);
	        				}
	        				comp.append(0);
	        			}
	        			else
	        			{
	        				comp.append(val);
	        				user.append(0);
	        			}
	        		}
	        	}
	        	save.append(comp.toString());
	        	save.append(" ");
	        	save.append(user.toString());
	        	save.close();
	    	} catch (IOException e)
	    	{
	    	}

	    	JOptionPane.showMessageDialog(null, "The puzzle was saved to the file:\n" + temp, "Saved", JOptionPane.INFORMATION_MESSAGE);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Invalid File Name/File Not Found", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Solves the board by creating a new SetSolve object
	 */
	public void solveSet(long wait)
	{
		boolean result = true;
		solveCount = 0;
		solveTime = System.currentTimeMillis();
		SetSolve i = new SetSolve(board, wait);
		try
		{
			solveCount = i.solvePuzzle(0);
		}
		catch (EmptyStackException e)
		{
			result = false;
		}
		solveTime = System.currentTimeMillis() - solveTime;

		if(result)
		{
			winGame();
		}
		else
		{
			loseGame();
		}
		setFocusAll();
	}

	/**
	 * Attempts to solve the puzzle by randomly entering
	 * values
	 *
	 * @return True if solved
	 */
	public boolean solveBrute()
	{
		ArrayList<Integer> valid = new ArrayList<Integer>(spaceList());
		int randRow = 0, randCol = 0, randVal = 0, count = 81;
		boolean result = true;
		solveCount = 0;
		solveTime = System.currentTimeMillis();

		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < N; j++)
			{
				if(!board[i][j].isModifiable())
				{
					count--;
					solveCount++;
				}
			}
		}

		while(count > 0)
		{
			randRow = valid.get((int)(Math.random() * N));
			randCol = valid.get((int)(Math.random() * N));
			randVal = 1 + (int)(Math.random() * N);

			if(board[randRow][randCol].isModifiable() && validMove(randRow, randCol, randVal))
			{
				board[randRow][randCol].setValue(randVal);
				count--;
			}
		}

		if(checkEmpty())
		{
			result = false;
		}

		solveTime = System.currentTimeMillis() - solveTime;

		if(result)
		{
			winGame();
		}
		else
		{
			loseGame();
		}

		setFocusAll();

		return result;
	}

	/**
	 * The solveStack uses a stack approach to fill in the
	 * remaining squares of a partially solved puzzle
	 *
	 * @return True if solved
	 */
	public boolean solveStack(long wait)
	{
		int row = 0, col = 0, value = 1;
		boolean moved = false, result = true;
		boolean pause = false;
		SudokuMove temp;
		solveCount = 0;
		solveTime = System.currentTimeMillis();

		if(wait == -1)
		{
			wait = 0;
			pause = true;
		}

		while(value <= N)
		{
			moved = false;

			if(row < N && col < N)
			{
				temp = board[row][col];
				if(temp.getValue() == value)
				{
					moved = true;
				}
				else if(temp.getValue() == 0 && validMove(row, col, value))
				{
					temp.setValue(value);
					tracker.push(temp);
					moved = true;
					solveCount++;
					try
					{
						Thread.sleep(wait);
					}
					catch (InterruptedException e) {}
				}
			}

			if(moved)
			{
				if(row < N - 1)
				{
					col = 0;
					row++;
				}
				else if(row == N - 1)
				{
					row = 0;
					col = 0;
					value++;
				}
			}
			else if (!moved)
			{
				if(col < N - 1)
				{
					col++;
				}
				else
				{
					if(tracker.size() == 0)
					{
						break;
					}
					else
					{
						if(pause)
						{
							JOptionPane.showMessageDialog(null, "A move has been popped", "POP!", JOptionPane.PLAIN_MESSAGE);
							pause = false;
						}
						temp = tracker.pop();
						row = temp.getRow();
						col = temp.getCol() + 1;
						value = temp.getValue();
						temp.setValue(0);
					}
				}
			}
		}

		if(checkEmpty())
		{
			result = false;
		}

		solveTime = System.currentTimeMillis() - solveTime;

		setFocusAll();
		if(result)
		{
			winGame();
		}
		else
		{
			loseGame();
		}

		return result;
	}

	/**
	 * Sets all the squares to unfocusable. Inteded for use with
	 * solver
	 */
	public void unFocusAll()
	{
		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				board[row][col].setFocusable(false);
			}
		}
	}

	/**
	 * The verifySudoku verifies that a solution is correct
	 * and returns a boolean indicating valid or invalid
	 *
	 * @return True if the solution is valid
	 */
	public boolean verifySudoku()
	{
		boolean result = true;

		for(int i = 0; i < N; i++)
		{
			if(!validRow(i) || !validCol(i) || !validCube(i))
			{
				result = false;
				break;
			}
		}

		if (checkEmpty())
		{
			result = false;
		}

		if(result)
		{
			JOptionPane.showMessageDialog(null, "Valid Solution", "", JOptionPane.PLAIN_MESSAGE);
		}
		else
		{
			keepPlaying();
		}
		return result;
	}

	//Private Static methods

	private static int getBoardSize()
	{
		Integer options[] = {4, 9, 16, 25, 36};
		int result = (Integer)JOptionPane.showInputDialog(null, "Select the size of the board", "UW Sudoku", JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

		return result;
	}

	//Private Methods

	/**
	 * The checkEmpty() is used to determine if there are
	 * any empty squares on the board
	 *
	 * @return True if empty squares exist
	 */
	private boolean checkEmpty()
	{
		boolean result = false;
		int row, col;

		for(row = 0; row < N; row++)
		{
			for(col = 0; col < N; col++)
			{
				if(board[row][col].getValue() == 0)
				{
					result = true;
					break;
				}
			}
			if(col < N)
			{
				break;
			}
		}

		return result;
	}

	//Private methods

	private void colorvert(int row, int col)
	{
		int cube = findCube(row, col);
		int bs = (int)Math.sqrt(N);
		int enterRow = 0, enterCol = 0;

		enterRow = (cube / bs) * bs;
		enterCol = (cube % bs) * bs;

		for(SudokuMove[] sa : board)
		{
			for(SudokuMove s : sa)
			{
				s.setBackground(BACKONE);
			}
		}

		for(int i = 0; i < N; i++)
		{
			board[i][col].setBackground(BACKTWO);
			board[row][i].setBackground(BACKTWO);
		}

		for(int i = enterRow; i < enterRow + bs ; i++)
		{
			for(int j = enterCol; j < enterCol + bs ; j++)
			{
				board[i][j].setBackground(BACKTWO);
			}
		}
	}

	/**
	 * The find cube method is used to determine the cube
	 * based on the row and column
	 *
	 * @param r The row
	 * @param c The column
	 * @return The cube number
	 */
	private int findCube(int r, int c)
	{
		int cube = 0;
		int bs = (int)Math.sqrt(N);

		cube = (c / bs) + bs * (r / bs);

		return cube;
	}

	/**
	 * Computes the list of avaliable colors for a square
	 *
	 * @param row The square's row
	 * @param col The square's column
	 * @return An arraylist of avaliable values for the square
	 */
	private ArrayList<Integer> getColors(int row, int col)
	{
		ArrayList<Integer> colors = new ArrayList<Integer>();
		int cube = findCube(row, col);
		int bs = (int)Math.sqrt(N);
		int enterRow = 0, enterCol = 0;

		enterRow = (cube / bs) * bs;
		enterCol = (cube % bs) * bs;

		for(int i = 1; i <= N; i++)
		{
			colors.add(i);
		}

		for(int i = 0; i < N; i++)
		{
			Integer ione = board[row][i].getValue();
			Integer itwo = board[i][col].getValue();
			if(colors.contains(ione))
			{
				colors.remove(ione);
			}
			if(colors.contains(itwo))
			{
				colors.remove(itwo);
			}
		}

		for(int i = enterRow; i < enterRow + 3 ; i++)
		{
			for(int j = enterCol; j < enterCol + 3 ; j++)
			{
				Integer ione = board[i][j].getValue();
				if(colors.contains(ione))
				{
					colors.remove(ione);
				}
			}
		}

		return colors;
	}

	/**
	 * Initializes the game
	 */
	private void init()
	{
		setTitle("UW Sudoku v1.5");
		int screenx = kit.getScreenSize().width;
		int screeny = kit.getScreenSize().height;

		gp = new JPanel();
		gp.setLayout(new GridLayout(N, N, 2, 2));
		gp.setBackground(BACKTHREE);
		initMoves();
		add(gp, BorderLayout.CENTER);

		m = new SudokuMenu(this);
		setJMenuBar(m);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		xwidth = gp.getSize().width;
		yheight = gp.getSize().height + 54;
		xpos = (int)((screenx - xwidth)/2);
		ypos = (int)((screeny - yheight)/2);
		setLocation(xpos, ypos);
		setResizable(true);
		setIconImage(new ImageIcon("src/assets/logo.gif").getImage());
		setVisible(true);
		System.out.println(xwidth);
		System.out.println(yheight);
		System.out.println(board[0][0].getSize());
	}

	/**
	 * Sets up the moves on the game panel
	 */
	private void initMoves()
	{
		Font font;
		SudokuMove newMove;

		for(int row = 0; row < N; row++)
		{
			for(int col = 0; col < N; col++)
			{
				newMove = new SudokuMove(row, col);
				newMove.setBackground(BACKONE);
				int temp = 3 * (kit.getScreenSize().height/N/4);
				newMove.setPreferredSize(new Dimension(temp, temp));
				font = new Font("Ariel", Font.PLAIN, (int)(temp*.75));
				/*
				if (row <= 2 && (col <= 2 || col > 5)){
					newMove.setBackground (BACKTWO);
				}
				if (row >2 && row <= 5 &&  col > 2 && col  <= 5){
					newMove.setBackground (BACKTWO);
				}
				if (row > 5 && ((col <= 2) || (col > 5))){
					newMove.setBackground (BACKTWO);
				}
				*/
				newMove.setValue(0);
				newMove.addKeyListener(new KeyListener(){
					public void keyPressed(KeyEvent arg0) {}
					public void keyReleased(KeyEvent arg0) {}
					public void keyTyped(KeyEvent e) {
						SudokuMove temp = (SudokuMove)(e.getSource());
						if(temp != null)
						{try{
							int i = Integer.parseInt(((Character)e.getKeyChar()).toString());
							temp.setValue(i);
							modCount++;
							if(hint){
								if(validMove(temp.getRow(), temp.getCol(), temp.getValue())){
									temp.setForeground(FOREUSER);}
								else{
									temp.setForeground(FOREERROR);}}}
							catch (NumberFormatException err){}}}});
				newMove.addMouseListener(new MouseListener(){
					public void mouseClicked(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent arg0) {}
					public void mouseExited(MouseEvent arg0) {}
					public void mousePressed(MouseEvent e) {
					if(MouseEvent.BUTTON3 == e.getButton())
					{
						JPopupMenu popup = new JPopupMenu();
						final SudokuMove temp = (SudokuMove)e.getSource();
						JMenuItem menuItem;
						ArrayList<Integer> list = getColors(temp.getRow(), temp.getCol());
					    for(final Integer i : list)
					    {
					    	menuItem = new JMenuItem(i.toString());
						    menuItem.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent arg0) {
									temp.setValue(i);
								}});
						    popup.add(menuItem);

					    }
						popup.show(e.getComponent(), e.getX(), e.getY());
					}
					else if(MouseEvent.BUTTON1 == e.getButton())
					{
						SudokuMove temp = (SudokuMove)(e.getSource());
						if(vert)
						{
							colorvert(temp.getRow(), temp.getCol());
						}
						if(temp.equals(getFocusOwner())){
							int i = temp.getValue();
							modCount++;
							if(i < N){
								temp.setValue(++i);}
							else{
								temp.setValue(0); }
							if(hint){
								if(validMove(temp.getRow(), temp.getCol(), temp.getValue())){
									temp.setForeground(FOREUSER);}
								else{
									temp.setForeground(FOREERROR);}}
							}}}
					public void mouseReleased(MouseEvent arg0) {}
					});
				newMove.setFont(font);
				newMove.setForeground(FOREUSER);
				newMove.setFocusable(true);
				newMove.setBorder(BorderFactory.createLineBorder(BORDERCOLOR));

				playTime = System.currentTimeMillis();
				board[row][col] = newMove;
				gp.add(newMove);
			}
		}
	}

	/**
	 * The keepPlaying method is called when the user requests
	 * verification on an invalid or incomplete puzzle
	 */
	private void keepPlaying()
	{
		StringBuilder s = new StringBuilder();
		double time = (double)(System.currentTimeMillis() - playTime)/1000.0;
		time = Math.floor((time*1000))/1000;

		s.append("You're not done yet!");
		s.append("\nYou have taken: ");
		s.append(time);
		s.append(" seconds, and ");
		s.append(modCount);
		s.append(" moves so far.");

		JOptionPane.showMessageDialog(null, s.toString(), "Keep Playing", JOptionPane.PLAIN_MESSAGE);

	}

	/**
	 * The loseGame method is called if the user loses the game
	 * or the puzzle is unsolvable
	 */
	private void loseGame()
	{
		StringBuilder s = new StringBuilder();
		double time = (double)(System.currentTimeMillis() - playTime)/1000.0;
		time = Math.floor((time*1000))/1000;

		s.append("Sorry the puzzle is unsolvable");
		if(solveCount == 0)
		{
			s.append("\nIt took: ");
			s.append(time);
			s.append(" seconds, and ");
			s.append(modCount);
			s.append(" moves to fail.");
		}
		else
		{
			s.append("\nIt took the computer: ");
			s.append(time);
			s.append(" seconds, and ");
			s.append(solveCount);
			s.append(" moves to fail.");
		}

		JOptionPane.showMessageDialog(null, s.toString(), "Game Over", JOptionPane.PLAIN_MESSAGE);

	}

	/**
	 * The solvable method checks if the current puzzle is solvable
	 *
	 * @return True if solvable
	 */
	private boolean solvable()
	{
		boolean result = true;

		try
		{
			SetSolve i = new SetSolve(board, 0);
			solveCount = i.solvePuzzle(150);
		}
		catch (EmptyStackException e)
		{
			result = false;
		}

		return result;
	}

	/**
	 * The spaceCollection method returns an Integer
	 * ArrayList containing the values 0 through N - 1
	 * corresponding to indecies for the rows columns
	 * and cubes
	 */
	private ArrayList<Integer> spaceList()
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < N; i++)
		{
			list.add(i);
		}

		return list;
	}

	/**
	 * The validCol method is used to determine if the
	 * current column is valid, ignoring empty squares
	 *
	 * @param col The column index
	 * @return True if valid
	 */
	private boolean validCol(int col)
	{
		ArrayList<Integer> num = new ArrayList<Integer> (valueList());
		boolean valid = true;

		for(int i = 0; i < N; i++)
		{
			Integer temp = board[i][col].getValue();
			if(temp != 0)
			{
				if(num.contains(temp))
				{
					num.remove(temp);
				}
				else
				{
					valid = false;
					break;
				}
			}
		}

		return valid;
	}

	/**
	 * The validCube method is used to determine if the
	 * current cube is valid, ignoring empty squares
	 *
	 * @param cube The cube index
	 * @return True if valid
	 */
	private boolean validCube(int cube)
	{
		ArrayList<Integer> num = new ArrayList<Integer> (valueList());
		int enterRow = 0, enterCol = 0;
		boolean valid = true;
		int bs = (int)Math.sqrt(N);

		enterRow = (cube / bs) * bs;
		enterCol = (cube % bs) * bs;

		for(int i = enterRow; i < enterRow + 3 ; i++)
		{
			for(int j = enterCol; j < enterCol + 3 ; j++)
			{
				Integer temp = board[i][j].getValue();
				if(temp != 0)
				{
					if(num.contains(temp))
					{
						num.remove(temp);
					}
					else
					{
						valid = false;
						break;
					}
				}
			}
		}

		return valid;
	}

	/**
	 * The validMove method is used to check if a certian
	 * move is valid
	 *
	 * @param row The row on the board
	 * @param col The column on the board
	 * @param move The value to place at the position
	 * @return True if valid
	 */
	private boolean validMove(int row, int col, int move)
	{
		int orig = 0;
		boolean result = false;

		if(board[row][col].isModifiable())
		{
			orig = board[row][col].getValue();

			board[row][col].setValue(move);

			if(validRow(row) && validCol(col) && validCube(findCube(row, col)))
			{
				result = true;
			}

			board[row][col].setValue(orig);
		}

		return result;
	}

	/**
	 * The validRow method is used to determine if the
	 * current row is valid, ignoring empty squares
	 *
	 * @param row The row index
	 * @return True if valid
	 */
	private boolean validRow(int row)
	{
		ArrayList<Integer> num = new ArrayList<Integer> (valueList());
		boolean valid = true;

		for(int i = 0; i < N; i++)
		{
			Integer temp = board[row][i].getValue();
			if(temp != 0)
			{
				if(num.contains(temp))
				{
					num.remove(temp);
				}
				else
				{
					valid = false;
					break;
				}
			}
		}

		return valid;
	}

	/**
	 * The valueList method returns an Integer ArrayList
	 * contining the valid entries on the puzzle
	 *
	 * @return Valid puzzle entries
	 */
	private ArrayList<Integer> valueList()
	{
		ArrayList<Integer> list = new ArrayList<Integer>();

		for(int i = 1; i <= N; i++)
		{
			list.add(i);
		}

		return list;
	}

	/**
	 * The winGame method is called when the puzzle is solved.
	 */
	private void winGame()
	{
		StringBuilder s = new StringBuilder();

		s.append("Congratulations!");
		if(solveCount == 0)
		{
			double time = (double)(System.currentTimeMillis() - playTime)/1000.0;
			time = Math.floor((time*1000))/1000;
			s.append("\nYou took: ");
			s.append(time);
			s.append(" seconds, and ");
			s.append(modCount);
			s.append(" moves.");
		}
		else
		{
			double time = (double)(solveTime)/1000.0;
			time = Math.floor((time*1000))/1000;
			s.append("\nIt took the computer: ");
			s.append(time);
			s.append(" seconds, and ");
			s.append(solveCount);
			s.append(" moves.");
		}

		JOptionPane.showMessageDialog(null, s.toString(), "Game Over", JOptionPane.PLAIN_MESSAGE);
	}
} //End of class SudokuGUI
