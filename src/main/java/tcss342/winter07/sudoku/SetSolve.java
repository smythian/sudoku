/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.Stack;

@SuppressWarnings("unchecked")
public class SetSolve
{
	//Private Fields

	/**
	 * The game board
	 */
	private SudokuMove[][] sm;

	/**
	 * The doubly dimensioned array of ArrayLists containing possible
	 * moves for each square on the board
	 */
	private LinkedList<Integer>[][] possibles;

	/**
	 * The stack of recent moves
	 */
	private Stack<IntersectMove> moveStack = new Stack<IntersectMove>();

	/**
	 * Solve count
	 */
	private int solveCount;

	/**
	 * Wait time
	 */
	private long wait;

	/**
	 * The max solve count
	 */
	private int maxCount;

	/**
	 * Constructs a new intersect solver
	 */
	public SetSolve(SudokuMove[][] sm, long wait)
	{
		this.sm = sm;
		this.wait = wait;
		possibles = new LinkedList[sm.length][sm.length];
	}

	//Public Methods

	/**
	 * Returns a solved puzzle
	 */
	public int solvePuzzle(int maxCount) throws EmptyStackException
	{
		solveCount = 0;

		solve();

		return solveCount;
	}

	//Private Methods

	private void delCol(int row, int col)
	{
		LinkedList<Integer> temp = possibles[row][col];

		for(int r = 0; r < sm.length; r++)
		{
			Integer val = sm[r][col].getValue();
			if(temp.contains(val))
			{
				temp.remove(val);
			}
		}
	}

	private void delCube(int row, int col)
	{
		LinkedList<Integer> temp = possibles[row][col];
		int cube = findCube(row, col);
		int bs = (int)Math.sqrt(sm.length);
		int enterRow = 0, enterCol = 0;

		enterRow = (cube / bs) * bs;
		enterCol = (cube % bs) * bs;

		for(int i = enterRow; i < enterRow + bs ; i++)
		{
			for(int j = enterCol; j < enterCol + bs ; j++)
			{
				Integer val = sm[i][j].getValue();
				if(temp.contains(val))
				{
					temp.remove(val);
				}
			}
		}
	}

	private void delRow(int row, int col)
	{
		LinkedList<Integer> temp = possibles[row][col];

		for(int c = 0; c < sm.length; c++)
		{
			Integer val = sm[row][c].getValue();
			if(temp.contains(val))
			{
				temp.remove(val);
			}
		}
	}

	/**
	 * Finds all valid moves for a square, applies a move to a square
	 * if it is the only move avaliable.
	 *
	 * @return 1 if a move was made, 0 if a square has no valid moves and -1 if no moves were made
	 */
	private int findIntersect()
	{
		int result = -1;
		int row = 0, col = 0;

		while(row < sm.length && col < sm.length)
		{
			if(sm[row][col].getValue() == 0)
			{
				delRow(row, col);
				delCol(row, col);
				delCube(row, col);
				int listSize = possibles[row][col].size();
				if(listSize == 1)
				{
					int val = possibles[row][col].poll();
					sm[row][col].setValue(val);
					IntersectMove im = new IntersectMove(sm[row][col], possibles[row][col], false);
					moveStack.push(im);
					try
					{
						Thread.sleep(wait);
					}
					catch (InterruptedException e) {}
					solveCount++;
					result = 1;
				}
				else if (listSize == 0)
				{
					result = 0;
					break;
				}
			}

			col++;

			if(col >= sm.length)
			{
				col = 0;
				row++;
			}
		}

		return result;
	}

	/**
	 * Creates an ArrayList containing the values 1-N for each
	 * square on the board
	 */
	private void initPossibles()
	{
		for(int row = 0; row < sm.length; row++)
		{
			for(int col = 0; col < sm[row].length; col++)
			{
				possibles[row][col] = new LinkedList<Integer>();
				for(int i = 1; i <= sm.length; i++)
				{
					possibles[row][col].add(i);
				}
			}
		}
	}

	/**
	 * Solves the puzzle
	 */
	private void solve() throws EmptyStackException
	{
		do
		{
			initPossibles();
			int takeGuess = findIntersect();

			if(takeGuess == -1)
			{
				pushMove();
			}
			else if(takeGuess == 0)
			{
				popMove();
			}

			if(maxCount > 0 && solveCount > maxCount)
			{
				throw new EmptyStackException();
			}
		}
		while(emptySquares());
	}

	/**
	 * Finds the square with the fewest possible options and sets
	 * it to the first value in the list and adds the move to the
	 * stack
	 */
	private void pushMove()
	{
		int size = sm.length + 1, row = 0, col = 0;
		for(int i = 0; i < sm.length; i++)
		{
			for(int j = 0; j < sm.length; j++)
			{
				if(possibles[i][j].size() < size && possibles[i][j].size() > 0)
				{
					row = i;
					col = j;
					size = possibles[i][j].size();
				}
			}
		}

		sm[row][col].setValue(possibles[row][col].poll());
		try
		{
			Thread.sleep(wait);
		}
		catch (InterruptedException e) {}
		solveCount++;
		IntersectMove im = new IntersectMove(sm[row][col], possibles[row][col], true);
		moveStack.push(im);
	}

	/**
	 * Pops the last move off the stack and tries the next move
	 * from in the list or pops another move.
	 */
	private void popMove() throws EmptyStackException
	{
		IntersectMove im;
		SudokuMove move;
		LinkedList poss;
		boolean guess = false;
		int val;

		do
		{
			im = moveStack.pop();
			move = im.sm;
			poss = im.mll;
			guess = im.guess;
			move.setValue(0);
		}
		while(!guess);

		if(poss.isEmpty())
		{
			popMove();
		}
		else
		{
			val = (Integer)poss.poll();
			move.setValue(val);
			im = new IntersectMove(move, poss, true);
			moveStack.push(im);
		}
	}

	/**
	 * The find cube method is used to determine the cube
	 * based on the row and column
	 *
	 * @param r The row
	 * @param c The column
	 * @return The cube number
	 */
	private int findCube(int r, int c)
	{
		int cube = 0;
		int bs = (int)Math.sqrt(sm.length);

		cube = (c / bs) + bs * (r / bs);

		return cube;
	}

	private boolean emptySquares()
	{
		boolean result = false;
		int row = 0, col = 0;

		while(row < sm.length && col < sm.length)
		{
			if(sm[row][col].getValue() == 0)
			{
				result = true;
				break;
			}

			col++;

			if(col >= sm.length)
			{
				row++;
				col = 0;
			}
		}

		return result;
	}

	private class IntersectMove
	{
		private SudokuMove sm;

		private LinkedList<Integer> mll;

		private boolean guess;

		private IntersectMove(SudokuMove sm, LinkedList<Integer> mll, boolean guess)
		{
			this.sm = sm;
			this.mll = mll;
			this.guess = guess;
		}
	}
}
