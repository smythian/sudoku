/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * The SavedGameFilter provides a menu dialog filter for saved Sudoku puzzles
 *
 * @author Eric Smyth
 */
public class SavedGameFilter extends FileFilter
{
	//Public Methods

	/**
	 * Returns true if the passed file is a saved
	 * Sudoku puzzle
	 */
	public boolean accept(File f)
	{
		boolean result = false;
		String extension = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (f.isDirectory()) {
            result = true;
        }

	    if (i > 0 &&  i < s.length() - 1)
	    {
	    	extension = s.substring(i+1).toLowerCase();
	    }

        if (extension != null) {
            if (extension.equals("ssg"))
            {
            	result = true;
            }
        }

		return result;
	}

	/**
	 * Description of the filter
	 */
	public String getDescription()
	{
		return "Saved Sudoku Games";
	}
} //End of class SavedGameFilter
