/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


/**
 * Runs the Sudoku program.
 *
 * @author Eric Smyth
 */
public class SudokuMain
{
	//Constructor

	/**
     * Constructs the main GUI window frame.
     *
     * @param args Command line arguments (ignored).
     */
	public static void main(String[] args)
	{
		// initialize GUI frame
		new SudokuGUI();
	}
} //End of class SudokuMain
