/*
 * UW Sudoku is licensed under the MIT License. See the LICENSE file for details.
 *
 * Copyright (C) 2007 Eric Smyth <https://smyth.app>
 */

package tcss342.winter07.sudoku;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

/**
 * The SudokuMenu class creates the menu for the Sudoku game and
 * provides event handeling
 *
 * @author Eric Smyth
 */
@SuppressWarnings("serial")
public class SudokuMenu extends JMenuBar
{
	//Private Methods

	/**
	 * The SudokuGUI which created the SudokuMenu
	 */
    private SudokuGUI s;

    //Constructor

    /**
     * Constructor for the SudokuMenu
     * @param s
     */
    public SudokuMenu (SudokuGUI s)
    {
    	this.s = s;
    	createMenuBar();
    }

	//Public Methods

	/**
     * The SudokuMenu bar for the Sudoku game
     */
    public void createMenuBar() {
        JMenu menu, submenu, subsubmenu;
        JMenuItem menuItem;
        JCheckBoxMenuItem cbmenuItem;

        //File SudokuMenu
        menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        add(menu);

        //New Game Sub Menu
        submenu = new JMenu("New Game");
        submenu.setMnemonic(KeyEvent.VK_N);

        //Set Creation Sub Menu
        subsubmenu = new JMenu("Caley Table Creation");

       //High Game Option
        menuItem = new JMenuItem("High Density");
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newCayley(1);}});
        subsubmenu.add(menuItem);

        //Medium Game Option
        menuItem = new JMenuItem("Medium Density");
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newCayley(2);}});
        subsubmenu.add(menuItem);

        //Low Game Option
        menuItem = new JMenuItem("Low Density", KeyEvent.VK_H);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newCayley(3);}});
        subsubmenu.add(menuItem);

        submenu.add(subsubmenu);

        //Set Creation Sub Menu
        subsubmenu = new JMenu("Color List Creation");

       //High Game Option
        menuItem = new JMenuItem("High Density");
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newGame(1);}});
        subsubmenu.add(menuItem);

        //Medium Game Option
        menuItem = new JMenuItem("Medium Density");
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newGame(2);}});
        subsubmenu.add(menuItem);

        //Low Game Option
        menuItem = new JMenuItem("Low Density", KeyEvent.VK_H);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newGame(3);}});
        subsubmenu.add(menuItem);

        submenu.add(subsubmenu);

        //Custom Game Option
        menuItem = new JMenuItem("Custom", KeyEvent.VK_C);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.newCustom();}});
        submenu.add(menuItem);

        //Pre generated game option
        menuItem = new JMenuItem("Pre Generated...", KeyEvent.VK_P);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.loadGame(getPreNum());}});
        submenu.add(menuItem);

        menu.add(submenu);

        menu.addSeparator();

        //Load Game Option
        menuItem = new JMenuItem("Open...",KeyEvent.VK_O);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.loadGame(0);}});
        menu.add(menuItem);

        //Save Game Option
        menuItem = new JMenuItem("Save Game...",KeyEvent.VK_S);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.save(true);}});
        menu.add(menuItem);

        //Save Game Option
        menuItem = new JMenuItem("Save Puzzle...",KeyEvent.VK_A);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.save(false);}});
        menu.add(menuItem);

        menu.addSeparator();

        //Quit Option
        menuItem = new JMenuItem("Exit",KeyEvent.VK_E);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.dispose();}});
        menu.add(menuItem);

        //Game SudokuMenu
        menu = new JMenu("Game");
        menu.setMnemonic(KeyEvent.VK_G);
        add(menu);

        //Clear Option
        menuItem = new JMenuItem("Clear Board",KeyEvent.VK_C);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.clear();}});
        menu.add(menuItem);

        //Reset All Option
        menuItem = new JMenuItem("Reset All",KeyEvent.VK_R);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.reset();}});
        menu.add(menuItem);

        //Provide Hints Option
        cbmenuItem = new JCheckBoxMenuItem("Provide Hints");
        cbmenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
    			s.hint(((JCheckBoxMenuItem)arg0.getSource()).getState());}});
           menu.add(cbmenuItem);

        //View Adjacent Vertices
        cbmenuItem = new JCheckBoxMenuItem("View Adjacent Vertices");
        cbmenuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		s.adjacent(((JCheckBoxMenuItem)arg0.getSource()).getState());}});
        menu.add(cbmenuItem);

        menu.addSeparator();

        //Solve Sub SudokuMenu
        submenu = new JMenu("Solve Puzzle");
        submenu.setMnemonic(KeyEvent.VK_S);

        //Set Solve Option
        menuItem = new JMenuItem("List Coloring",KeyEvent.VK_I);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		s.unFocusAll();
        		new Thread(new Intersect(0)).start();}});
        submenu.add(menuItem);

        //Slow Set Solve Option
        menuItem = new JMenuItem("List Coloring - Slow",KeyEvent.VK_N);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		s.unFocusAll();
        		new Thread(new Intersect(10)).start();}});
        submenu.add(menuItem);

        //Random/Brute Solve Option
        menuItem = new JMenuItem("Random",KeyEvent.VK_R);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		s.unFocusAll();
        		s.solveBrute();}});
        submenu.add(menuItem);

        //Stack Solve Option
        menuItem = new JMenuItem("Stack",KeyEvent.VK_S);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		s.unFocusAll();
        		new Thread(new Solve(0)).start();}});
        submenu.add(menuItem);

        //Pause Stack Solve Option
        menuItem = new JMenuItem("Stack - Pause",KeyEvent.VK_P);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		new Thread(new Solve(-1)).start();
        		s.unFocusAll();}});
        submenu.add(menuItem);

        //Slow Stack Solve Option
        menuItem = new JMenuItem("Stack - Slow",KeyEvent.VK_P);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
        		new Thread(new Solve(100)).start();
        		s.unFocusAll();}});
        submenu.add(menuItem);

        menu.add(submenu);

        //Check Option
        menuItem = new JMenuItem("Check Solution",KeyEvent.VK_H);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.verifySudoku();}});
        menu.add(menuItem);

        //Build the Help menu.
        menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_H);
        add(menu);

        //Help Option
        menuItem = new JMenuItem("Contents...",KeyEvent.VK_C);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.help();}});
        menu.add(menuItem);

        menu.addSeparator();

        //About Option
        menuItem = new JMenuItem("About...",KeyEvent.VK_A);
        menuItem.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent arg0) {
			s.about();}});
        menu.add(menuItem);
	}

    private class Solve extends Thread
    {
    	private int count;
    	protected Solve(int count)
    	{
    		this.count = count;
    	}

    	public void run()
    	{
    		s.solveStack(count);
    	}
    }

    private class Intersect extends Thread
    {
    	private int count;
    	protected Intersect(int count)
    	{
    		this.count = count;
    	}

    	public void run()
    	{
    		s.solveSet(count);
    	}
    }

    private int getPreNum()
    {
    	String result;

    	result = JOptionPane.showInputDialog(null, "Select a puzzle\n1-400 - Easy\n401-700" +
    			" - Medium\n701-1000 - Hard", "Open Pre Generated Puzzle",
    			JOptionPane.QUESTION_MESSAGE);

    	return Integer.parseInt(result);
    }
} //End of class SudokuMenu
