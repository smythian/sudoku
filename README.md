# UW Sudoku

Sudoku project for TCSS 342 (Data Structures)

## Getting Started

### Prerequisites

* [Java JDK 1.6+](https://openjdk.java.net/)
* [Maven](https://maven.apache.org)

### Commands

* Compile: `mvn compile`
* Package: `mvn package`
* Install: `mvn install`
* Clean: `mvn clean`
* Run: `java -jar target/sudoku-1.5.0.jar`

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/mastiff/lurcher/tags). 

## Authors

* [Eric Smyth](https://gitlab.com/smythian)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
